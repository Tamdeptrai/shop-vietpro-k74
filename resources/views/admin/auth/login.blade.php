<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<link href="{{ asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{ asset('admin/css/datepicker3.css')}}" rel="stylesheet">
	<link href="{{ asset('admin/css/styles.css')}}" rel="stylesheet">
</head>

<body>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" action ="{{ route('admin.login') }}" method="POST">
						@csrf
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="email" type="text" autofocus="">
							</div>
							<small style="color: red" class="errors-email">
								@if($errors->has('email'))
								{{$errors->first('email')}}

								@endif
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
							</div>
							<small style="color: red" class="errors-pass">
							<div class="form-group">
									<input class="form-control" placeholder="Confirm Pass" name="confirm" type="password" value="">
								</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<input type="submit" value="login" class="btn btn-primary" >
							{{-- <a href="index.html" class="btn btn-primary">Login</a> --}}
						</fieldset>
					</form>
					@php
						if($errors->any()){
							foreach ($errors->all() as $loi) {
								echo "".$loi;
								# code...
							}
						}	
					@endphp
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			console.log('hihi');
			$('.btn').click(function(e){
				e.preventDefault();
			});
			let data = {
				email:$('input[name=email]').val(),
				password:$('input[name=password]').val(),
				token:$('input[name=_token]').val()
			}
			$.ajax({
				url:'/admin/login',
				method:'POST',
				data:data,
				success: function(success){
					if(success.errors.email){
						$('.errors-email').html(success.errors.email);
					}
					if(success.errors.password){
						$('.errors-pass').html(success.errors.password);
					}
				}
			});
		})
	</script>
</body>

</html>