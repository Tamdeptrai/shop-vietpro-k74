<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    /**
     * Show checkout form
     */
    public function index() {
        return view('client.checkout');
    }

    /**
     * Send checkout to server
     */
    public function verify() {
        return 'Send checkout to server';
    }

    /**
     * Show complete form
     */
    public function complete() {
        return view('client.complete');
    }

    
}
