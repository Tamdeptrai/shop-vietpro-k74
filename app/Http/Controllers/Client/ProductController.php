<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * List product
     */
    public function list() {
        return view('client.shop');
    }

    /**
     * Product detail
     */
    public function detail(Request $request) {
        return view('client.detail');
    }
}
